﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace Json
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();


            string directory = AppDomain.CurrentDomain.BaseDirectory + "../../Asmenuduomenys/";

            Random r = new random;

            

            Asmuo asmuo = new Asmuo()
            {
                Vardas = "Jonas",
                Pavarde = "Jonaitis",
                GimimoData = new DateTime(1959, 12, 19),
                Atlyginimas = 580.36,
                Ugis = 183,
            };
            
            Adresas adresas = new Adresas()
            {
                Miestas = "Vilnius",
                Gatve = "Belekokia",
                NamoNr = 7,
            };

           

            asmuo.Adresas = adresas;

            var json = JsonConvert.SerializeObject(asmuo);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            var fileName = $"{asmuo.Vardas}_{asmuo.Pavarde}_duomenys.json";

            File.WriteAllText(directory + fileName, json);


        }
    }
    public class Asmuo
    {
        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public DateTime GimimoData { get; set; }

        public double Atlyginimas { get; set; }

        public int Ugis { get; set; }

        public Adresas Adresas { get; set; }

    }

    public class Adresas
    {
        public string Miestas { get; set; }

        public string Gatve { get; set; }

        public int NamoNr { get; set; }

    }
    public class PajamosIslaidos
    {

        public string Menesis { get; set; }

        public int Pajamos { get; set; }

        public int Islaidos { get; set; }

    }
}

